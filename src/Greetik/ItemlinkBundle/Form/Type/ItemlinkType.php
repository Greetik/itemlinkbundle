<?php

namespace Greetik\ItemlinkBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemlinkType
 *
 * @author Paco
 */
class ItemlinkType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('link')
                ->add('name');
    }

    public function getName() {
        return 'Itemlink';
    }

    public function getDefaultOptions(array $options) {
        return array('data_class' => 'Greetik\BlogBundle\Entity\Itemlink');
    }

}
