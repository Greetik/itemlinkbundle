<?php

namespace Greetik\ItemlinkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Itemlink
 *
 * @ORM\Table(name="itemlink", indexes={
 *      @ORM\Index(name="itemtype", columns={"itemtype"}),  @ORM\Index(name="itemid", columns={"itemid"}),  @ORM\Index(name="numorder", columns={"numorder"}),  @ORM\Index(name="item", columns={"itemid", "itemtype"}),  @ORM\Index(name="itemorder", columns={"itemid", "itemtype", "numorder"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\ItemlinkBundle\Repository\ItemlinkRepository")
 */
class Itemlink
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="itemid", type="integer")
     */
    private $itemid;

   /**
     * @var integer
     *
     * @ORM\Column(name="numorder", type="integer")
     */
    private $numorder;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Itemlink
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     *
     * @return Itemlink
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set itemid
     *
     * @param integer $itemid
     *
     * @return Itemlink
     */
    public function setItemid($itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return integer
     */
    public function getItemid()
    {
        return $this->itemid;
    }

    /**
     * Set numorder
     *
     * @param integer $numorder
     *
     * @return Itemlink
     */
    public function setNumorder($numorder)
    {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer
     */
    public function getNumorder()
    {
        return $this->numorder;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Itemlink
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
