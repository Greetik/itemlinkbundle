<?php

namespace Greetik\ItemlinkBundle\Services;

use Symfony\Component\Finder\Exception\AccessDeniedException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Itemlink Tools
 *
 * @author Pacolmg
 */
class Itemlinks {

    private $em;
    private $beinterface;

    public function __construct($_entityManager, $_beinterface) {
        $this->em = $_entityManager;
        $this->beinterface= $_beinterface;
    }

   
    public function getItemlinksByItem($item_id, $item_type) {
        return $this->em->getRepository('ItemlinkBundle:Itemlink')->getItemlinksByItem($item_id, $item_type);
    }

    public function getItemlinkObject($id) {
            return $this->em->getRepository('ItemlinkBundle:Itemlink')->findOneById($id);
    }

    public function getItemlink($id) {
            return $this->em->getRepository('ItemlinkBundle:Itemlink')->getItemlink($id);
    }

    public function getItemlinkByLink($link) {
            return $this->em->getRepository('ItemlinkBundle:Itemlink')->findOneByLink($link);
    }

    public function modifyItemlink($itemlink) {
        $this->em->persist($itemlink);
        $this->em->flush();
    }

    public function insertItemlink($itemlink) {
        $itemlink->setNumorder($this->em->getRepository('ItemlinkBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype())+1);
        $this->em->persist($itemlink);
        $this->em->flush();
    }

    public function deleteItemlink($itemlink) {
        if (is_numeric($itemlink)) $itemlink = $this->getItemlinkObject ($itemlink);
        $this->em->remove($itemlink);
        $this->em->flush();
        
        $this->beinterface->reorderItemElems($this->em->getRepository('ItemlinkBundle:Itemlink')->findBy(array('itemid'=>$itemlink->getItemid(), 'itemtype'=>$itemlink->getItemtype())), array('numorder'=>'ASC'));
    }


    public function putupItemlink($itemlink){
        $this->beinterface->moveItemElem('ItemlinkBundle:Itemlink', $itemlink->getId(), $itemlink->getNumorder()-2, $itemlink->getNumorder(), $this->em->getRepository('ItemlinkBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype()), $itemlink->getItemtype(), $itemlink->getItemid() );
    }
    
    public function putdownItemlink($itemlink){
        if ($itemlink->getNumorder() < $this->em->getRepository('ItemlinkBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype()))
        $this->beinterface->moveItemElem('ItemlinkBundle:Itemlink', $itemlink->getId(), $itemlink->getNumorder(), $itemlink->getNumorder(), $this->em->getRepository('ItemlinkBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype()), $itemlink->getItemtype(), $itemlink->getItemid() );
    }
    
    
}
