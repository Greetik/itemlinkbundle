<?php

namespace Greetik\ItemlinkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\ItemlinkBundle\Entity\Itemlink;
use Greetik\ItemlinkBundle\Form\Type\ItemlinkType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
    * Show the new link insert form
    * 
    * @author Pacolmg
    */
     public function insertformAction($item_id, $item_type, $id='')
     {
        
         if (!empty($id)){$itemlink=$this->get('itemlink.tools')->getItemlinkObject($id);}
         else{
            $itemlink=new Itemlink();
         }
         
         $newForm = $this->createForm(ItemlinkType::class, $itemlink);

        return $this->render('ItemlinkBundle:Default:insert.html.twig',array('new_form' => $newForm->createView(), 'item_id' => $item_id, 'item_type'=>$item_type,'id' =>$id));
    }

    /**
    * Edit the data of an itemlink or insert a new one
    * 
    * @param int $id is received by Get Request
    * @param Itemlink $item is received by Post Request
    * @author Pacolmg
    */
     public function insertmodifyAction(Request $request, $item_id, $item_type, $id=''){
        $item = $request->get('itemlink');
      
        if (!empty($id)){
            $itemlink=$this->get('itemlink.tools')->getItemlinkObject($id);
            $editing = true;
        }else{ $itemlink = new Itemlink(); $editing=false;}

         $editForm = $this->createForm(ItemlinkType::class, $itemlink);
         $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            if ($editing){
                try{
                    $this->get($this->getParameter('itemlink.permsservice'))->modifyItemlink($itemlink);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }
            }else{
                try{
                    $itemlink->setItemtype($item_type);
                    $itemlink->setItemid($item_id);
                    //$itemlink->setNumorder(0);
                    $this->get($this->getParameter('itemlink.permsservice'))->insertItemlink($itemlink);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }                
            }
            
            return $this->render('ItemlinkBundle:Default:index.html.twig', array('itemlinks' => $this->get($this->getParameter('itemlink.permsservice'))->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }

     /**
    * Delete a itemlink
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function dropAction($id)
     {
        $itemlink = $this->get('itemlink.tools')->getItemlinkObject($id);
        
       try{
        $this->get($this->getParameter('itemlink.permsservice'))->deleteItemlink($itemlink);
       }catch(\Exception $e){
           return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
       }
       
        return $this->render('ItemlinkBundle:Default:index.html.twig', array('itemlinks' => $this->get($this->getParameter('itemlink.permsservice'))->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
     }
     
     public function putupAction(Request $request){
        
         $itemlink=$this->get('itemlink.tools')->getItemlinkObject($request->get('id'));
         try{
            $this->get($this->getParameter('itemlink.permsservice'))->putupItemlink($itemlink);
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
         return $this->render('ItemlinkBundle:Default:index.html.twig', array('itemlinks' => $this->get($this->getParameter('itemlink.permsservice'))->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
     }
     
     public function putdownAction(Request $request){
         $itemlink=$this->get('itemlink.tools')->getItemlinkObject($request->get('id'));
         try{
            $this->get($this->getParameter('itemlink.permsservice'))->putdownItemlink($itemlink);
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
          return $this->render('ItemlinkBundle:Default:index.html.twig', array('itemlinks' => $this->get($this->getParameter('itemlink.permsservice'))->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
     }
}
