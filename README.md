# README #


### What is this repository for? ###

* ItemlinkBundle is a bundle to add links to items: every item is a pair item_id/item_type.

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before make a upload or edit a file.

##Add it to AppKernel.php.##
new \Greetik\ItemlinkBundle\ItemlinkBundle()


##In the config.yml you can add the service##
itemlink:
    permsservice: app.itemlinks
